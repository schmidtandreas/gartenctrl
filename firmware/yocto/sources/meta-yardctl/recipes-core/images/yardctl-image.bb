SUMMARY = "Garden controll image based on core-image-minimal"

COMPATIBLE_MACHINE = "^rpi$"

CORE_IMAGE_EXTRA_INSTALL:append = " \
    kernel-modules \
    linux-firmware-bcm43430 \
    i2c-tools \
    wpa-supplicant \
"

IMAGE_INSTALL = " \
    packagegroup-core-boot \
    ${CORE_IMAGE_EXTRA_INSTALL} \
"

DISTRO_FEATURES:append = " \
    systemd \
    wifi \
"

IMAGE_FEATURES += " \
    splash \
"

IMAGE_LINGUAS = " "

INIT_MANAGER = "systemd"
VIRTUAL-RUNTIME:syslog = ""
VIRTUAL-RUNTIME:base-utils-syslog = ""

LICENSE = "MIT"

inherit core-image

IMAGE_ROOTFS_SIZE ?= "8192"
IMAGE_ROOTFS_EXTRA_SPACE:append = "${@bb.utils.contains("DISTRO_FEATURES", "systemd", " + 4096", "", d)}"
